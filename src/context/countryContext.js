import axios from 'axios';
import createContext from "./createContext";


const initialState = {
    "count": 0,
    "countryResult": {},
    "globalState": { "confirmed": 0, "deaths": 0, "recovered": 0 },
    "dateWiseState" : []

}

const countryReducer = (state, action) => {
    switch (action.type) {
        case 'getcountryState':
            console.log("action.payload:", action.payload)
            const dateKey = Object.keys(action.payload.result)[0];
            return {
                ...state,
                count: action.payload.count,
                countryResult: {
                    date: dateKey,
                    confirmed: action.payload.result[dateKey].confirmed,
                    deaths: action.payload.result[dateKey].deaths,
                    recovered: action.payload.result[dateKey].recovered
                }
            };
        case 'get_Global_state_error':
            console.log("Payload: ", action.payload)
            return {
                ...state
            };
        case 'getCountyWiseState':
            const data = Object.keys(action.payload.result).map(i => {
                const date = i;
                return {
                    date: i,
                    confirmed: action.payload.result[i].confirmed,
                    deaths: action.payload.result[i].deaths,
                    recovered: action.payload.result[i].recovered
                }
            });

            return {
                ...state,
                dateWiseState: data
            };
        case 'get_Global_County_state_error':
            console.log("Payload: ", action.payload)
            return {
                ...state
            };
        default:
            return state;
    }
}

const getcountryState = (dispatch) => async ({ country }) => {
    try {
        let url = `https://covidapi.info/api/v1/country/${country}/latest`;
        const response = await axios.get(url)
        dispatch({ type: 'getcountryState', payload: response.data });
    } catch (error) {
        dispatch(getcountryStateError(error.response.data));
    };
};

const getcountryStateError = (resData) => {
    return {
        type: "get_Global_state_error", payload: { resData }
    }
};
const getCountyWiseState = (dispatch) => async ({ country }) => {
    try {
        let url = `https://covidapi.info/api/v1/country/${country}`;
        const response = await axios.get(url)
        dispatch({ type: 'getCountyWiseState', payload: response.data });
    } catch (error) {
        dispatch(getCountyWiseStateError(error.response.data));
    };
};

const getCountyWiseStateError = (resData) => {
    return {
        type: "get_Global_County_state_error", payload: resData
    }
};



export const { Context, Provider } = createContext(
    countryReducer,
    { getcountryState, getCountyWiseState },
    { ...initialState }
);
