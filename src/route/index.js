import React from "react";
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';
import {HomeView, CountryDetailsView} from '../components/Pages';



const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/home' component={HomeView} />
            <Route exact path='/home/:country' component={CountryDetailsView} />
            <Route path='*' exact render={() => <Redirect to='/home' />} />
        </Switch>
    </BrowserRouter>
);

export default Routes;