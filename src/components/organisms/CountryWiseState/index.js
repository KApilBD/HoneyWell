import React from "react";
import { Link } from 'react-router-dom';


import MaterialTable from 'material-table';
import styled from "styled-components";

const TableWrapper = styled.div`
    max-width: 600px;
    margin:auto;
`;

const CountryWiseState = ({ countryData }) => {

    console.log("globalCountryState", countryData.result)

    const data = countryData.result.map((i) => {
        let country = Object.keys(i)[0];
        return { country: country, confirmed: i[country].confirmed, deaths: i[country].deaths, recovered: i[country].recovered }
    })

    // console.log("DATA:", data)

    return (
        <div>
            <h3>Country State List</h3>
            <p>Gloabl Count: {countryData.count}     |     Date: {countryData.date}</p>
            <TableWrapper>
                <MaterialTable
                    columns={[
                        { title: 'Country', field: 'country', render: rowData => <Link to={`/home/${rowData.country}`} style={{ textDecoration: 'none' }}>{rowData.country}</Link> },
                        { title: 'Confirmed', field: 'confirmed', type: 'numeric' },
                        { title: 'Deaths', field: 'deaths', type: 'numeric' },
                        { title: 'Recovered', field: 'recovered', type: 'numeric' }
                    ]}
                    data={data}
                    title="Country List"
                />
            </TableWrapper>
        </div>
    )
};

export default CountryWiseState;