import React, { useContext, useEffect } from "react";
import { useParams } from 'react-router-dom';
import MaterialTable from 'material-table';
import styled from "styled-components";
import { Context as CountryContext } from "../../../context/countryContext";


const TableWrapper = styled.div`
    max-width: 600px;
    margin:auto;
`;


const StateWrapper = styled.div`
    display: flex;
    justify-content: space-around;
    max-width: 800px;
    margin: auto;
`;

const CountryDetailsView = () => {
    const { country } = useParams();
    console.log("USE Param: ", country);

    const { state, getcountryState, getCountyWiseState } = useContext(CountryContext);
    console.log("State, ", state)
    useEffect(() => {
        getcountryState({ country })
        getCountyWiseState({ country })
    }, [])

    return (
        <div>
            <h1>{country} state</h1>
            <StateWrapper>
                <p>Confirmed: {state.countryResult.confirmed}</p>
                <p>Deaths: {state.countryResult.deaths}</p>
                <p>Recovered: {state.countryResult.recovered}</p>
            </StateWrapper>
            <hr />
            <TableWrapper>
                <MaterialTable
                    columns={[
                        { title: 'Date', field: 'date'},
                        { title: 'Confirmed', field: 'confirmed', type: 'numeric' },
                        { title: 'Deaths', field: 'deaths', type: 'numeric' },
                        { title: 'Recovered', field: 'recovered', type: 'numeric' }
                    ]}
                    data={state.dateWiseState}
                    title="Datewise country State"
                />
            </TableWrapper>
        </div>
    );
}
export default CountryDetailsView;