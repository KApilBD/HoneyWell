import HomeView from "./Home";
import CountryDetailsView from "./CountryDetails";


export {
    HomeView,
    CountryDetailsView
};