import React, { useContext, useEffect } from "react";

import { Context as GlobalStateContext } from "../../../context/globalContext";
import { CountryWiseState } from "../../organisms"

const HomeView = () => {
    const { state, getGlobalState, getGlobalCountyState } = useContext(GlobalStateContext);

    // console.log("state", state)

    useEffect(() => {
        getGlobalState();
        getGlobalCountyState();
    }, [])

    return (
        <div>
            <h1>Covid-19 Global </h1>
            <p>Gloabl Count: {state.count}     |     Date: {state.date}</p>
            <hr />
            <h2> Global State </h2>
            <p>Confirmed: {state.globalState.confirmed}</p>
            <p>Deaths: {state.globalState.deaths}</p>
            <p>Recovered: {state.globalState.recovered}</p>
            <CountryWiseState countryData={state.globalCountryState} />

        </div>
    )
};

export default HomeView;