import axios from 'axios';
import createContext from "./createContext";


const initialState = {
    "count": 0,
    "date": "",
    "globalState": { "confirmed": 0, "deaths": 0, "recovered": 0 },
    "globalCountryState": {
        "count": 0,
        "date": "",
        "result": [],
    }

}

const globalReducer = (state, action) => {
    switch (action.type) {
        case 'get_Global_state':
            return {
                ...state
            };
        case 'get_Global_state_success':
            const { resData } = action.payload;
            return {
                ...state,
                count: resData.count,
                date: resData.date,
                globalState: { ...resData.result }
            };
        case 'get_Global_state_error':
            console.log("Payload: ", action.payload)
            return {
                ...state
            };
        case 'get_Global_County_state_success':

            console.log("Payload Country: ", action.payload)

            return {
                ...state,
                globalCountryState: {
                    ...state.globalCountryState,
                    ...action.payload
                }
            };
        case 'get_Global_County_state_error':
            console.log("Payload: ", action.payload)
            return {
                ...state
            };
        default:
            return state;
    }
}

const getGlobalState = (dispatch) => {
    return () => {
        let url = "https://covidapi.info/api/v1/global";
        axios.get(url).then(res => {
            // console.log("Response", res)
            dispatch(getGlobalStateSuccess(res.data));
        }).catch((error) => {
            dispatch(getGlobalStateError(error.response.data));
        })
    };
}
const getGlobalStateSuccess = (resData) => {
    return {
        type: "get_Global_state_success", payload: { resData }
    }
};
const getGlobalStateError = (resData) => {
    return {
        type: "get_Global_state_error", payload: { resData }
    }
};
const getGlobalCountyState = (dispatch) => {
    return () => {
        let url = "https://covidapi.info/api/v1/global/latest";
        axios.get(url).then(res => {
            // console.log("Response", res)
            dispatch(getGlobalCountyStateSuccess(res.data));
        }).catch((error) => {
            dispatch(getGlobalCountyStateError(error.response.data));
        })
    };
}
const getGlobalCountyStateSuccess = (resData) => {
    return {
        type: "get_Global_County_state_success", payload: resData
    }
};
const getGlobalCountyStateError = (resData) => {
    return {
        type: "get_Global_County_state_error", payload: resData
    }
};



export const { Context, Provider } = createContext(
    globalReducer,
    { getGlobalState, getGlobalCountyState },
    { ...initialState }
);
